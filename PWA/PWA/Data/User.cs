﻿using Microsoft.AspNetCore.Identity;
using Microsoft.VisualBasic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Net;

#pragma warning disable CS8765, CS8618, CS8603
namespace PWA.Data;

public class User : IdentityUser<Guid>
{

    [StringLength(50)]
    public string FirstName { get; set; }

    [StringLength(50)]
    public string LastName { get; set; }

    [StringLength(50)]
    public override string Email
    {
        get => base.Email;
        set
        {
            base.Email = value ?? throw new NullReferenceException(nameof(Email));
            base.UserName = value;
        }
    }

    [StringLength(50)]
    public override string UserName
    {
        get => base.Email;
        set => throw new NotSupportedException();
    }
    private User()
    { }

    public User(string email)
    {
        Email = email;
    }
}
