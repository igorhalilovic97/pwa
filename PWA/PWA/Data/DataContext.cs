﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace PWA.Data;

public class DataContext : DbContext
{
    public const string Schema = null;


    public DbSet<User> User { get; set; }


    public DataContext(DbContextOptions<DataContext> options)
       : base(options)
    { }
    private static void Configure(EntityTypeBuilder<User> builder)
    {
        builder.ToTable("users");
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        builder.HasCharSet("Utf8mb4");
        builder.HasDefaultSchema(Schema);

        base.OnModelCreating(builder);

        Configure(builder.Entity<User>());
    }
}

