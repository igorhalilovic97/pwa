using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PWA.Data;

public class Program
{
    private static WebApplicationBuilder GetBuilder()
        => WebApplication.CreateBuilder(new WebApplicationOptions()
        {
            WebRootPath = Path.Combine(Directory.GetCurrentDirectory(), "Content")
        });

    private static void AddDbContext(WebApplicationBuilder builder)
    {
        builder.Services
            .AddDbContext<DataContext>(options =>
            {
                options.UseMySql(
                    builder.Configuration.GetConnectionString("Default"),
                    new MySqlServerVersion(new Version(8, 0)),
                    mySqlOptions =>
                    {
                        mySqlOptions.MigrationsHistoryTable("_migrations", DataContext.Schema);
                    });
            });
    }

    private static void AddIdentity(WebApplicationBuilder builder)
    {
        builder.Services.AddDbContext<DataContext>();
        builder.Services.AddIdentity<IdentityUser,IdentityRole>()
                        .AddEntityFrameworkStores<DataContext>()
                        .AddDefaultTokenProviders();

        builder.Services.
            AddIdentityCore<User>(o =>
            {
                if (!builder.Environment.IsProduction())
                {
                    o.Stores.MaxLengthForKeys = 128;

                    o.Password.RequireNonAlphanumeric = false;
                    o.Password.RequiredLength = 6;
                    o.Password.RequireDigit = false;
                    o.Password.RequiredUniqueChars = 0;
                    o.Password.RequireUppercase = false;
                    o.Password.RequireLowercase = false;

                    o.SignIn.RequireConfirmedAccount = true;
                }
            })
            .AddSignInManager();
    }

    private static void UseProxyHeaders(WebApplication app)
    {
        if (app.Configuration.GetValue<bool>("UseProxyHeaders"))
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
    }

    private static void UsePagesAndControllers(WebApplication app)
    {
        app.UseAuthentication();
        app.UseAuthorization();
        app.MapRazorPages();
        app.MapControllers();
    }

    public static void Main(string[] args)
    {
        var builder = GetBuilder();

        AddDbContext(builder);
        AddIdentity(builder);

        var app = builder.Build();

        app.UseExceptionHandler("/error/1");
        app.UseStatusCodePagesWithReExecute("/error/{0}");

        if (app.Environment.IsProduction())
            app.UseHsts();

        app.UseStaticFiles();
        app.UseRouting();

        UseProxyHeaders(app);
        UsePagesAndControllers(app);

        app.Run();
    }
}
