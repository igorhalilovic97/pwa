using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace PWA.Pages
{
    public class ContactModel : PageModel
    {

        public string Email { get; set; }

        public string Message { get; set; }

        public string Name { get; set; }

        public void OnGet()
        {
        }
    }
}
